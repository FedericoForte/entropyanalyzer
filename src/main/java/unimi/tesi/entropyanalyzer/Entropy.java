package unimi.tesi.entropyanalyzer;

public class Entropy {
	
	public static double calculate(double[] prob){
		if(prob.length != 256)
			throw new IllegalArgumentException("Array length < 256");
		double ent = 0;
		for(int i = 0; i < 256; i++){
			//check valid probability
			if(prob[i] < 0 || prob[i] > 1)
				throw new IllegalArgumentException("Error probability");
			if (prob[i] > 0)
				/*H(x) = p(x)*log_2(1/p(x)) = 
				 * -p(x)*log_2(p(x)) =
				 * -p(x)*log(p(x))/log(2)
				 */
				ent -= prob[i] * Math.log(prob[i])/Math.log(2);
		}	
		return ent;
	}

}
