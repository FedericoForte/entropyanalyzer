package unimi.tesi.entropyanalyzer;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class FileHandler {
	
	private File file;
	
	public FileHandler(File f){
		file = f;
	}
	
	public double analysis() throws IOException {
		long totalBytes = 0;
		long[] combCount = new long[256];			//count number of occurrences(1 byte => 2^8=256 combinations of different bytes)
		double[] prob = new double[256];			//count probability associated to every combinations
		byte[] contentFile;
		InputStream in = new FileInputStream(file);
		int available = in.available(); 
		//read file
        contentFile = new byte[ available ];  
        new DataInputStream(in).readFully(contentFile);
        in.close();
        for(byte b : contentFile){
        	combCount[(b & 0xff)]++;
        	totalBytes++;
        }
        
        //calculate probability for every possible byte
    	for (int i = 0; i < 256; i++)
    		prob[i] = ((double) combCount[i]) / ((double)totalBytes);	
    	
    	//calculate entropy
    	return Entropy.calculate(prob);
	}
}
