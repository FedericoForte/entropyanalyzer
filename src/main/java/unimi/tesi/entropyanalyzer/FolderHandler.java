package unimi.tesi.entropyanalyzer;

import java.io.File;
import java.util.ArrayList;

public class FolderHandler{
	
	//listRes contains the result
    public void listFiles(String path, ArrayList<File> listRes){
        File root = new File(path);
        File[] list = root.listFiles();
        if (list == null) return;
        for ( File f : list )
            if (f.isDirectory())
            	listFiles(f.getAbsolutePath(), listRes);
            else
                listRes.add(f);
    }
}    
