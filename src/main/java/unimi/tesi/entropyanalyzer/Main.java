package unimi.tesi.entropyanalyzer;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

public class Main {
	
	private static final int MIN_ENTROPY_ENCRYPTED_FILE = 7;
	private static double sumEntropy = 0;
	private static long numEntropyFiles = 0;
	private static long numTotFiles = 0;
	private static long numLogFiles = 0;
	private static long numXmlFiles = 0;
	private static long numImageFiles = 0;
	private static long numAudioFiles = 0;
	private static long numVideoFiles = 0;
	private static long numOther = 0;
	private static ArrayList<String> encryptedFiles = new ArrayList<String>();
	private static ArrayList<String> databaseFiles = new ArrayList<String>();
	
	public static void main(String[] arg){
		if(arg.length!=1){
			System.out.println("Error number of parameters .");
			return;
		}
		File file = new File(arg[0]);
		if(!file.exists()){
			System.out.println("Directory doesn't exist.");
			return;
		}else if(!file.isDirectory()){
			System.out.println("Not a directory.");
			return;
		}
		
		ArrayList<File> listFiles = new ArrayList<File>();
		new FolderHandler().listFiles(arg[0], listFiles);
		for(File f : listFiles){
			try {
				calculateStats(Files.probeContentType(Paths.get(f.getAbsolutePath())), f);
			} catch (IOException e) {
				System.out.println("Error file: " + f.getAbsolutePath());
			}
		}
		
		System.out.println("Statistics"
				+"\nNumber of files: " + numTotFiles 
				+"\nMean entropy*: " + sumEntropy/((double)numEntropyFiles)
				+"\nLog files: " + numLogFiles
				+"\nXml files: " + numXmlFiles
				+"\nImage files: " + numImageFiles
				+"\nAudio files: " + numAudioFiles
				+"\nVideo files: " + numVideoFiles
				+"\nProbably encrypted or compressed files: " + encryptedFiles.size());
		
		//Encrypted files
		if(encryptedFiles.size()!=0)
			System.out.println("Probably encrypted or compressed files list: ");
		for(int i=0; i < encryptedFiles.size(); i++)
			System.out.println(encryptedFiles.get(i));
		
		//Database
		System.out.println("Database: " + databaseFiles.size());
		if(databaseFiles.size()!=0)
			System.out.println("Database list: ");
		for(int i=0; i < databaseFiles.size(); i++)
			System.out.println(databaseFiles.get(i));
		
		//Other
		System.out.println("Other files: " + numOther
				+"\n*:excluded audio, video, image files.");
			
		
		
	}

	private static void calculateStats(String probeContentType, File file) throws IOException {
		FileHandler handler = new FileHandler(file);
		double temp = 0;
		if(probeContentType.startsWith("text/x-log")){
			numEntropyFiles++;
			temp = handler.analysis();
			sumEntropy+=temp;
			if(temp > MIN_ENTROPY_ENCRYPTED_FILE)
				encryptedFiles.add(file.getAbsolutePath());
			else
				numLogFiles++;
		}else if(probeContentType.startsWith("application/xml")){
			numEntropyFiles++;
			temp = handler.analysis();
			sumEntropy+=temp;
			if(temp > MIN_ENTROPY_ENCRYPTED_FILE)
				encryptedFiles.add(file.getAbsolutePath());
			else
				numXmlFiles++;
		}else if(probeContentType.startsWith("image/")){
			numImageFiles++;
		}else if(probeContentType.startsWith("audio/")){
			numAudioFiles++;	
		}else if(probeContentType.startsWith("video/")){
			numVideoFiles++;	
		}else if(probeContentType.startsWith("application/x-sqlite3")){
			numEntropyFiles++;
			temp = handler.analysis();
			sumEntropy+=temp;
			if(temp > MIN_ENTROPY_ENCRYPTED_FILE)
				encryptedFiles.add(file.getAbsolutePath());
			else
				databaseFiles.add(file.getAbsolutePath());
		}else{
			numEntropyFiles++;
			temp = handler.analysis();
			sumEntropy+=temp;
			if(temp > MIN_ENTROPY_ENCRYPTED_FILE)
				encryptedFiles.add(file.getAbsolutePath());
			else
				numOther++;
		}
		numTotFiles++;		
	}

}
