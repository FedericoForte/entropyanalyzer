package unimi.tesi.entropyanalyzer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.Test;

public class TestEntropy {
	
	private static final double DELTA = 1e-15;
	
	@Test
	public void testCalculateException(){
		try{
			Entropy.calculate(new double[5]);
			fail();
		}catch(IllegalArgumentException e){
			assertEquals(e.getMessage(), "Array length < 256");
			
		}
	}
	
	@Test
	public void testCalculateException2(){
		try{
			double[] array = new double[256];
			array[250] = 2;
			Entropy.calculate(array);
			fail();
		}catch(IllegalArgumentException e){
			assertEquals(e.getMessage(), "Error probability");
			
		}
	}
	
	@Test
	public void testCalculateEntropyZero(){
			double[] array = new double[256];
			assertEquals(0, Entropy.calculate(array), DELTA);
			array[100] = 1;
			assertEquals(0, Entropy.calculate(array), DELTA);
	}
	
	@Test
	public void testCalculateMaxEntropy(){
			double[] array = new double[256];
			for(int i = 0; i < 256; i++)
				array[i] = 1.0/256.0;
			assertEquals(8, Entropy.calculate(array), DELTA);
	}

}
