package unimi.tesi.entropyanalyzer;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;

import org.junit.Test;

public class TestFileHandler {
	
	private static final double DELTA = 1e-15;
	
	@Test
	public void testZeroEntropy() throws IOException {
		FileHandler f = new FileHandler(new File("src/test/java/unimi/tesi/entropyanalyzer/FileTest/ZeroEntropy.txt"));
		assertEquals(0, f.analysis() , DELTA);
	}
	
	@Test
	public void testTwoSymbols() throws IOException {
		FileHandler f = new FileHandler(new File("src/test/java/unimi/tesi/entropyanalyzer/FileTest/twoSymbols.txt"));
		assertEquals(1, f.analysis() , DELTA);
	}
	
	@Test
	public void testEncrypted() throws IOException {
		FileHandler f = new FileHandler(new File("src/test/java/unimi/tesi/entropyanalyzer/FileTest/entropy.c.aes"));
		assertTrue(f.analysis() > 7);
	}
}
