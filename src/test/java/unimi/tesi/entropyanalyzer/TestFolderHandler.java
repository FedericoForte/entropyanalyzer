package unimi.tesi.entropyanalyzer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import java.io.File;
import java.util.ArrayList;

public class TestFolderHandler {
	
	@Test
	public void testConstructor(){
		FolderHandler f = new FolderHandler();
		assertTrue(f!=null);
	}
	
	@Test
	public void testListFiles() {
		FolderHandler f = new FolderHandler();
		ArrayList<File> l = new ArrayList<File>();
		f.listFiles("src/test/java/unimi/tesi/entropyanalyzer/FolderTest",l);
		assertEquals(6, l.size());
		for(File file : l)
			assertTrue(file.getAbsolutePath().matches(".*.txt"));
	}
	
	

}
